<?php
namespace modules;

use yii\base\Module;
use craft\web\twig\variables\CraftVariable;
use yii\base\Event;

class ModuleHook extends Module
{
    public function init()
    {
        Event::on(CraftVariable::class, CraftVariable::EVENT_INIT, function(Event $e) {
            /** @var CraftVariable $variable */
            $variable = $e->sender;
    
            // Attach a behavior:
            $variable->attachBehaviors([
                ItemHelper::class,
            ]);

            // Attach a behavior:
            $variable->attachBehaviors([
                WebHelper::class,
            ]);
        });
    }
}