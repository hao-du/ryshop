<?php

namespace modules;

use yii\base\Behavior;

class WebHelper extends Behavior
{
    public function getName()
    {
        return 'WebHelper class';
    }

    public function getJsonCookie($cookieName)
    {
        if (!isset($_COOKIE[$cookieName]))
        {
            return null;
        }

        $cookieAsString = $_COOKIE[$cookieName];

        if(!isset($cookieAsString)){
            return null;
        }

        return json_decode($cookieAsString);
    }
}
