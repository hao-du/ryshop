<?php

namespace modules;

use yii\base\Behavior;

class ItemHelper extends Behavior
{
    public function getName()
    {
        return 'ItemHelper class';
    }

    //Get Url from Category Item, Internal Item or External Item
    public function getItemUrl($item)
    {
        $url = null;
        $queryString = '';

        if (isset($item->generalUrlQueryString) && !empty($item->generalUrlQueryString)) {
            $queryString = '?' . $item->generalUrlQueryString;
        }

        if (isset($item->generalUrl) && !empty($item->generalUrl)) {
            $url = $item->generalUrl;
        } elseif (isset($item->generalUrlItem) && count($item->generalUrlItem) > 0) {
            $url = '/' . $item->generalUrlItem->one()->uri;
        } elseif (isset($item->productCategory) && count($item->productCategory) > 0) {
            $url = '/' . $item->productCategory->inReverse()->one()->uri;
        }

        if (!empty($url)) {
            $url = $url . $queryString;
        }

        return $url;
    }

    //Check whether product has promotion or not
    public function hasPromotion($productItem)
    {
        if(!$productItem->promotionalStartDate || !$productItem->promotionalEndDate) {
            return false;
        }

        $format = 'Y-m-d H:i:s';

        $startDate = strtotime($productItem->promotionalStartDate->format($format));
        $endDate = strtotime($productItem->promotionalEndDate->format($format));
        $currentDate = strtotime(date($format));

        # Has Promotion
        if ($productItem->stock
            && $productItem->hasPromotion
            && $startDate <= $currentDate and $currentDate <= $endDate ) {
            return true;
        }

        return false;
    }
}
