var header = {
	setAffix: function () {
		$('.header').affix();
	},

	init: function () {
		$('.header #inputDesktopSearch, #headerSearchModel #inputMobileSearch').on('keyup', function (e) {
			if (e.keyCode == 13) {
				var keyword = this.value;
				header.search(keyword);
			}
		});

		$('#headerSearchModel #buttonMobileSearch').on('click', function () {
			var keyword = $('#headerSearchModel #inputMobileSearch').val();
			header.search(keyword);
		});
	},

	search: function (keyword) {
		if (!keyword || !searchPageUrl) {
			return;
		}

		var url = searchPageUrl;
		url = plugins.url.appendParamToUrl(url, 'keyword', keyword);
		window.location.href = url;
	}
};

var carorselBanner = {
	init: function () {
		$('.carousel-banner .owl-carousel').owlCarousel({
			loop: true,
			center: true,
			responsiveClass: true,
			items: 1,
			autoplay: true,
			autoplayTimeout: 3000,
			autoplaySpeed: 1500,
			autoplayHoverPause: true,
		});
	}
};

var carorselProducts = {
	init: function () {
		var responsive = {};
		responsive[common.screen.xs] = { items: 2, slideBy: 2 };
		responsive[common.screen.lg] = { items: 4, slideBy: 4 };
		responsive[common.screen.xl] = { items: 5, slideBy: 5 };

		var $carousel = $('.carousel-products .owl-carousel');

		$carousel.owlCarousel({
			loop: false,
			responsiveClass: true,
			responsive: responsive,
			slideBy: 1,
		});
	}
};

var productDetails = {
	init: function () {
		var $details = $('.product-details .owl-gallery');

		$details.owlCarouselGallery({enableZoom : false});
	}
};

var productList = {
	init: function () {
		productList.populateFilterValues();

		$('.product-list #buttonOpenProductFilterPopup, .product-list #buttonCloseFilterPopup').on('click', function () {
			var popupid = $(this).data('popup-id');

			$(popupid).toggleClass('in');

			$('.product-list .filter').toggleClass('in');
		});

		$('.product-list #selectSort').on('change', function () {
			productList.filterProducts();
		});

		$('.product-list #buttonApplyFilter').on('click', function () {
			productList.filterProducts();
		});

		$('.product-list .pagination .page-item .page-link').on('click', function () {
			var pageIndex = $(this).attr('tabindex');

			productList.filterProducts(pageIndex);
		});
	},

	populateFilterValues: function () {
		var sort = plugins.url.queryString['sort'];
		if (sort) {
			$('.product-list #selectSort').val(sort);
		}

		var promotionIds = plugins.url.queryString['promoids'];
		if (promotionIds) {
			promotionIds = promotionIds.split(',');
			promotionIds.forEach(function (element) {
				$('.product-list #collapsePromotionItems input[type="checkbox"][value="' + element + '"]').prop('checked', true);
			});
		}

		var categoryIds = plugins.url.queryString['catids'];
		if (categoryIds) {
			categoryIds = categoryIds.split(',');
			categoryIds.forEach(function (element) {
				$('.product-list #collapseCategoryItems input[type="checkbox"][value="' + element + '"]').prop('checked', true);
			});
		}

		var priceRanges = plugins.url.queryString['ranges'];
		if (priceRanges) {
			priceRanges = priceRanges.split(',');
			priceRanges.forEach(function (element) {
				$('.product-list #collapsePriceItems input[type="radio"][value="' + element + '"]').prop('checked', true);
			});
		}

		var index = plugins.url.queryString['index'];
		if (index) {
			$('.product-list .pagination .page-item .page-link').each(function () {
				var $link = $(this);

				if ($link.attr('tabindex') == index) {
					$link.parent().addClass('active');
					return;
				}
			});
		}
	},

	filterProducts: function (pageIndex) {
		var sort = $('.product-list #selectSort').children('option:selected').val();

		var checkboxCategories = $('.product-list #collapsePromotionItems input[type="checkbox"]:checked');
		var promotionIds = '';
		checkboxCategories.each(function () {
			var checkbox = this;
			promotionIds = plugins.utilities.appendString(promotionIds, checkbox.value, ',');
		});

		var checkboxCategories = $('.product-list #collapseCategoryItems input[type="checkbox"]:checked');
		var categoryIds = '';
		checkboxCategories.each(function () {
			var checkbox = this;
			categoryIds = plugins.utilities.appendString(categoryIds, checkbox.value, ',');
		});

		var checkboxPriceRanges = $('.product-list #collapsePriceItems input[type="radio"]:checked');
		var priceRanges = '';
		checkboxPriceRanges.each(function () {
			var checkbox = this;
			priceRanges = plugins.utilities.appendString(priceRanges, checkbox.value, ',');
		});

		var url = plugins.url.getCurrentURL(false);
		url = plugins.url.appendParamToUrl(url, 'keyword', plugins.url.queryString['keyword']);
		url = plugins.url.appendParamToUrl(url, 'sort', sort != '0' ? sort : '');
		url = plugins.url.appendParamToUrl(url, 'index', pageIndex ? pageIndex : '');

		if (promotionIds) {
			url = plugins.url.appendParamToUrl(url, 'promoids', promotionIds);
		}
		if (categoryIds) {
			url = plugins.url.appendParamToUrl(url, 'catids', categoryIds);
		}
		if (priceRanges) {
			url = plugins.url.appendParamToUrl(url, 'ranges', priceRanges);
		}
		window.location.href = url;
	}
};

var productTracking = {
	trackProducts : function() {
		

		$('.btn-view-product').on('click', function() {
			var id = $(this).data('id');

			var jsonProductIds = Cookies.get('productIds');

			var productIds = [];
			if(jsonProductIds){
				productIds = JSON.parse(jsonProductIds);
			}
			
			if (productIds.indexOf(id) >= 0) {
				return;
			}

			if(productIds.length == 10) {
				productIds.shift();
			}
			productIds.push(id);

			Cookies.set('productIds', JSON.stringify(productIds), { expires: 60 });
		});
	}
}