(function ($) {
    $.fn.affix = function () {
        this.each(function () {
            var $this = $(this);
            var affixElementOffsetTop = this.offsetTop;

            $(window).scroll(function () {
                if (window.pageYOffset > affixElementOffsetTop) {
                    $this.addClass("sticky");
                } else {
                    $this.removeClass("sticky");
                }
            });
        });
    };

    $.fn.owlCarouselGallery = function (options) {
        var defaultOpttions = {
            enableZoom: true,
            syncedSecondary: true,
            bigImageLoop: true,
            navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>']
        }

        if (options) {
            $.extend(defaultOpttions, options);
        }

        this.each(function () {
            var $this = $(this);

            var bigImageId = $this.data("big-image");
            var thumbnailsId = $this.data("thumbnails");

            var bigimage = $this.find(bigImageId);
            var thumbs = $this.find(thumbnailsId);

            bigimage
                .owlCarousel({
                    items: 1,
                    slideSpeed: 2000,
                    nav: true,
                    dots: false,
                    autoHeight:true,
                    touchDrag: !defaultOpttions.enableZoom,
                    mouseDrag: !defaultOpttions.enableZoom,
                    loop: defaultOpttions.bigImageLoop,
                    responsiveRefreshRate: 200,
                    navText: defaultOpttions.navText
                })
                .on("changed.owl.carousel", syncBigImagePosition);

            if(defaultOpttions.enableZoom){
                bigimage.find('.owl-item').zoom({ magnify: 0.6 });
            }

            thumbs
                .on("initialized.owl.carousel", function () {
                    thumbs
                        .find(".owl-item")
                        .eq(0)
                        .addClass("current");
                })
                .owlCarousel({
                    items: 4,
                    dots: true,
                    nav: false,
                    navText: defaultOpttions.navText,
                    smartSpeed: 200,
                    slideSpeed: 500,
                    slideBy: 4,
                    responsiveRefreshRate: 100
                })
                .on("changed.owl.carousel", syncThumbnailPosition)
                .on("click", ".owl-item", function (e) {
                    e.preventDefault();
                    var number = $(this).index();
                    bigimage.data("owl.carousel").to(number, 300, true);
                });

            function syncBigImagePosition(el) {
                //if loop is set to false, then you have to uncomment the next line
                if (!defaultOpttions.bigImageLoop) {
                    var current = el.item.index;
                }

                //to disable loop, comment this block
                //console.log(el);
                var count = el.item.count - 1;
                var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

                if (current < 0) {
                    current = count;
                }
                if (current > count) {
                    current = 0;
                }
                //to this
                thumbs
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                var onscreen = thumbs.find(".owl-item.active").length - 1;
                //console.log(onscreen)
                var start = thumbs
                    .find(".owl-item.active")
                    .first()
                    .index();
                var end = thumbs
                    .find(".owl-item.active")
                    .last()
                    .index();
                //console.log(end);
                if (current > end) {
                    thumbs.data("owl.carousel").to(current, 100, true);
                }
                if (current < start) {
                    thumbs.data("owl.carousel").to(current - onscreen, 100, true);
                }
            }

            function syncThumbnailPosition(el) {
                if (defaultOpttions.syncedSecondary) {
                    var number = el.item.index;
                    bigimage.data("owl.carousel").to(number, 100, true);
                }
            }
        });
    }
}(jQuery));

var plugins = {
    initFacebookChat: function () {
        window.fbAsyncInit = function () {
            FB.init({
                xfbml: true,
                version: 'v6.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    },

    url : {
        getCurrentURL: function (includeParameter) {
            if (includeParameter) {
                return window.location.href;
            }

            return window.location.origin + window.location.pathname;
        },

        appendParamToUrl: function (url, param, value, isOverrideIfExisted) {
            var hasNoParameter = url.indexOf('?') < 0;
            var paramName_assign = param + "=";
            
            // if value is not null, encode it
            value = value != null && value.length > 0
                        ? encodeURIComponent(value).replace(/%20/g, '+')
                        : value;

            if (hasNoParameter) {
                if (value) {
                    // if there's no parameter, and parameter value is valid
                    // feel free to add parameter with question mark
                    url += "?" + paramName_assign + value;
                }

                return url;
            }

            // now, url already has parameters
            var paramIndex = url.indexOf(paramName_assign);
            if (paramIndex < 0) {
                if (value) {
                    // if this parameter hasn't been defined yet, and its value is valid
                    // let's add parameter to query string with '&' prefix
                    url += "&" + paramName_assign + value;
                }

                return url;
            }

            // now, that parameter alreay exists
            if (!isOverrideIfExisted) {
                return url;
            }

            // let's:
            // - override that parameter if value is valid
            // - remove if invalid

            var tempStartIndex = paramIndex;
            var tempEndIndex = paramIndex + paramName_assign.length;

            while (url[tempEndIndex] != "&" && tempEndIndex < url.length) {
                tempEndIndex++;
            }

            if (value) {
                var tempStrToReplace = url.substring(tempStartIndex, tempEndIndex);
                url = url.replace(tempStrToReplace, paramName_assign + value);
            }
            else {
                // -1 to remove prefix ? or &
                var tempStrToReplace = url.substring(tempStartIndex - 1, tempEndIndex);
                url = url.replace(tempStrToReplace, "");
            }

            return url;
        },

        //Usage: plugins.url.queryString["param"]
        queryString: (function (a) {
            if (a == "") return {};
            var b = {};
            for (var i = 0; i < a.length; ++i) {
                var p = a[i].split('=');
                if (p.length != 2) continue;
                b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
            return b;
        })(window.location.search.substr(1).split('&')),

        encodeUrlComponent: function (value) {
            if (value) {
                return encodeURIComponent(value).replace(/%20/g, '+');
            }

            return value;
        },
    },

    utilities : {
        appendString: function(sourceString, inputString, separator, suffix){
            if(!inputString) {
                return sourceString;
            }

            if(!sourceString) {
                return inputString;
            }

            if(!separator) {
                separator = '';
            }

            if(suffix) {
                return sourceString + inputString + separator;
            }

            return sourceString + separator + inputString;
        },
    },
}