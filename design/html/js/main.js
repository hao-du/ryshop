if ($) {
    $(document).ready(function () {
        if (!plugins) {
            return;
        }

        if (header) {
            header.init();
            header.setAffix();
        }

        //Init Modules
        if (carorselBanner) {
            carorselBanner.init();
        }
        if (carorselProducts) {
            carorselProducts.init();
        }
        if(productDetails) {
            productDetails.init();
        }
        if(productList) {
            productList.init();
        }
        if(productTracking) {
            productTracking.trackProducts();
        }

        //Last Run
        plugins.initFacebookChat();
    });
}