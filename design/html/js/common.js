var common = {
    screen: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200
    },

    screenMode : function() {
        var width = $(window).width(); 

        if(width >= common.screen.xl){
            return common.screen.xl;
        }
        else if(width >= common.screen.lg){
            return common.screen.lg;
        }
        else if(width >= common.screen.md){
            return common.screen.md;
        }
        else if(width >= common.screen.sm){
            return common.screen.sm;
        }
        else {
            return common.screen.xs;
        }
    }
}